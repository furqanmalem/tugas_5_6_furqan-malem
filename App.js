import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ToastAndroid,
  StatusBar,
  ScrollView,
  StyleSheet,
} from 'react-native';
import {FlatGrid} from 'react-native-super-grid';
import Modal from 'react-native-modal';
import Clipboard from '@react-native-clipboard/clipboard';
import Icon from 'react-native-vector-icons/FontAwesome5';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      indexColor: 0,
      colorList: [
        {
          name: 'Red',
          baseColor: '#ff1744',
          color: [
            {hexCode: '#ffebee'},
            {hexCode: '#ffcdd2'},
            {hexCode: '#ef9a9a'},
            {hexCode: '#e57373'},
            {hexCode: '#ef5350'},
            {hexCode: '#f44336'},
            {hexCode: '#e53935'},
            {hexCode: '#d32f2f'},
            {hexCode: '#c62828'},
            {hexCode: '#b71c1c'},
            {hexCode: '#ff8a80'},
            {hexCode: '#ff5252'},
            {hexCode: '#ff1744'},
            {hexCode: '#d50000'},
          ],
        },
        {
          name: 'Pink',
          baseColor: '#ff4081',
          color: [
            {hexCode: '#fce4ec'},
            {hexCode: '#f8bbd0'},
            {hexCode: '#f48fb1'},
            {hexCode: '#f06292'},
            {hexCode: '#ec407a'},
            {hexCode: '#e91e63'},
            {hexCode: '#d81b60'},
            {hexCode: '#c2185b'},
            {hexCode: '#ad1457'},
            {hexCode: '#880e4f'},
            {hexCode: '#ff80ab'},
            {hexCode: '#ff4081'},
            {hexCode: '#f50057'},
            {hexCode: '#c51162'},
          ],
        },
        {
          name: 'Purple',
          baseColor: '#9c27b0',
          color: [
            {hexCode: '#f3e5f5'},
            {hexCode: '#e1bee7'},
            {hexCode: '#ce93d8'},
            {hexCode: '#ba68c8'},
            {hexCode: '#ab47bc'},
            {hexCode: '#9c27b0'},
            {hexCode: '#8e24aa'},
            {hexCode: '#7b1fa2'},
            {hexCode: '#6a1b9a'},
            {hexCode: '#4a148c'},
            {hexCode: '#ea80fc'},
            {hexCode: '#e040fb'},
            {hexCode: '#d500f9'},
            {hexCode: '#aa00ff'},
          ],
        },
        {
          name: 'DeepPurple',
          baseColor: '#673ab7',
          color: [
            {hexCode: '#ede7f6'},
            {hexCode: '#d1c4e9'},
            {hexCode: '#b39ddb'},
            {hexCode: '#9575cd'},
            {hexCode: '#7e57c2'},
            {hexCode: '#673ab7'},
            {hexCode: '#5e35b1'},
            {hexCode: '#512da8'},
            {hexCode: '#4527a0'},
            {hexCode: '#311b92'},
            {hexCode: '#b388ff'},
            {hexCode: '#7c4dff'},
            {hexCode: '#651fff'},
            {hexCode: '#6200ea'},
          ],
        },
        {
          name: 'Indigo',
          baseColor: '#3f51b5',
          color: [
            {hexCode: '#e8eaf6'},
            {hexCode: '#c5cae9'},
            {hexCode: '#9fa8da'},
            {hexCode: '#7986cb'},
            {hexCode: '#5c6bc0'},
            {hexCode: '#3f51b5'},
            {hexCode: '#3949ab'},
            {hexCode: '#303f9f'},
            {hexCode: '#283593'},
            {hexCode: '#1a237e'},
            {hexCode: '#8c9eff'},
            {hexCode: '#536dfe'},
            {hexCode: '#3d5afe'},
            {hexCode: '#304ffe'},
          ],
        },
        {
          name: 'Blue',
          baseColor: '#2196f3',
          color: [
            {hexCode: '#e3f2fd'},
            {hexCode: '#bbdefb'},
            {hexCode: '#90caf9'},
            {hexCode: '#64b5f6'},
            {hexCode: '#42a5f5'},
            {hexCode: '#2196f3'},
            {hexCode: '#1e88e5'},
            {hexCode: '#1976d2'},
            {hexCode: '#1565c0'},
            {hexCode: '#0d47a1'},
            {hexCode: '#82b1ff'},
            {hexCode: '#448aff'},
            {hexCode: '#2979ff'},
            {hexCode: '#2962ff'},
          ],
        },
        {
          name: 'LightBlue',
          baseColor: '#03a9f4',
          color: [
            {hexCode: '#e1f5fe'},
            {hexCode: '#b3e5fc'},
            {hexCode: '#81d4fa'},
            {hexCode: '#4fc3f7'},
            {hexCode: '#29b6f6'},
            {hexCode: '#03a9f4'},
            {hexCode: '#039be5'},
            {hexCode: '#0288d1'},
            {hexCode: '#0277bd'},
            {hexCode: '#01579b'},
            {hexCode: '#80d8ff'},
            {hexCode: '#40c4ff'},
            {hexCode: '#00b0ff'},
            {hexCode: '#0091ea'},
          ],
        },
        {
          name: 'Cyan',
          baseColor: '#00bcd4',
          color: [
            {hexCode: '#e0f7fa'},
            {hexCode: '#b2ebf2'},
            {hexCode: '#80deea'},
            {hexCode: '#4dd0e1'},
            {hexCode: '#26c6da'},
            {hexCode: '#00bcd4'},
            {hexCode: '#00acc1'},
            {hexCode: '#0097a7'},
            {hexCode: '#00838f'},
            {hexCode: '#006064'},
            {hexCode: '#84ffff'},
            {hexCode: '#18ffff'},
            {hexCode: '#00e5ff'},
            {hexCode: '#00b8d4'},
          ],
        },
        {
          name: 'Teal',
          baseColor: '#009688',
          color: [
            {hexCode: '#e0f2f1'},
            {hexCode: '#b2dfdb'},
            {hexCode: '#80cbc4'},
            {hexCode: '#4db6ac'},
            {hexCode: '#26a69a'},
            {hexCode: '#009688'},
            {hexCode: '#00897b'},
            {hexCode: '#00796b'},
            {hexCode: '#00695c'},
            {hexCode: '#004d40'},
            {hexCode: '#a7ffeb'},
            {hexCode: '#64ffda'},
            {hexCode: '#1de9b6'},
            {hexCode: '#00bfa5'},
          ],
        },
        {
          name: 'Green',
          baseColor: '#4caf50',
          color: [
            {hexCode: '#e8f5e9'},
            {hexCode: '#c8e6c9'},
            {hexCode: '#a5d6a7'},
            {hexCode: '#81c784'},
            {hexCode: '#66bb6a'},
            {hexCode: '#4caf50'},
            {hexCode: '#43a047'},
            {hexCode: '#388e3c'},
            {hexCode: '#2e7d32'},
            {hexCode: '#1b5e20'},
            {hexCode: '#b9f6ca'},
            {hexCode: '#69f0ae'},
            {hexCode: '#00e676'},
            {hexCode: '#00c853'},
          ],
        },
        {
          name: 'LightGreen',
          baseColor: '#8bc34a',
          color: [
            {hexCode: '#f1f8e9'},
            {hexCode: '#dcedc8'},
            {hexCode: '#c5e1a5'},
            {hexCode: '#aed581'},
            {hexCode: '#9ccc65'},
            {hexCode: '#8bc34a'},
            {hexCode: '#7cb342'},
            {hexCode: '#689f38'},
            {hexCode: '#558b2f'},
            {hexCode: '#33691e'},
            {hexCode: '#ccff90'},
            {hexCode: '#b2ff59'},
            {hexCode: '#76ff03'},
            {hexCode: '#64dd17'},
          ],
        },
        {
          name: 'Lime',
          baseColor: '#cddc39',
          color: [
            {hexCode: '#f9fbe7'},
            {hexCode: '#f0f4c3'},
            {hexCode: '#e6ee9c'},
            {hexCode: '#dce775'},
            {hexCode: '#d4e157'},
            {hexCode: '#cddc39'},
            {hexCode: '#c0ca33'},
            {hexCode: '#afb42b'},
            {hexCode: '#9e9d24'},
            {hexCode: '#827717'},
            {hexCode: '#f4ff81'},
            {hexCode: '#eeff41'},
            {hexCode: '#c6ff00'},
            {hexCode: '#aeea00'},
          ],
        },
        {
          name: 'Yellow',
          baseColor: '#ffeb3b',
          color: [
            {hexCode: '#fffde7'},
            {hexCode: '#fff9c4'},
            {hexCode: '#fff59d'},
            {hexCode: '#fff176'},
            {hexCode: '#ffee58'},
            {hexCode: '#ffeb3b'},
            {hexCode: '#fdd835'},
            {hexCode: '#fbc02d'},
            {hexCode: '#f9a825'},
            {hexCode: '#f57f17'},
            {hexCode: '#ffff8d'},
            {hexCode: '#ffff00'},
            {hexCode: '#ffea00'},
            {hexCode: '#ffd600'},
          ],
        },
        {
          name: 'Amber',
          baseColor: '#ffc107',
          color: [
            {hexCode: '#fff8e1'},
            {hexCode: '#ffecb3'},
            {hexCode: '#ffe082'},
            {hexCode: '#ffd54f'},
            {hexCode: '#ffca28'},
            {hexCode: '#ffc107'},
            {hexCode: '#ffb300'},
            {hexCode: '#ffa000'},
            {hexCode: '#ff8f00'},
            {hexCode: '#ff6f00'},
            {hexCode: '#ffe57f'},
            {hexCode: '#ffd740'},
            {hexCode: '#ffc400'},
            {hexCode: '#ffab00'},
          ],
        },
        {
          name: 'Orange',
          baseColor: '#ff9800',
          color: [
            {hexCode: '#fff3e0'},
            {hexCode: '#ffe0b2'},
            {hexCode: '#ffcc80'},
            {hexCode: '#ffb74d'},
            {hexCode: '#ffa726'},
            {hexCode: '#ff9800'},
            {hexCode: '#fb8c00'},
            {hexCode: '#f57c00'},
            {hexCode: '#ef6c00'},
            {hexCode: '#e65100'},
            {hexCode: '#ffd180'},
            {hexCode: '#ffab40'},
            {hexCode: '#ff9100'},
            {hexCode: '#ff6d00'},
          ],
        },
        {
          name: 'DeepOrange',
          baseColor: '#ff5722',
          color: [
            {hexCode: '#fbe9e7'},
            {hexCode: '#ffccbc'},
            {hexCode: '#ffab91'},
            {hexCode: '#ff8a65'},
            {hexCode: '#ff7043'},
            {hexCode: '#ff5722'},
            {hexCode: '#f4511e'},
            {hexCode: '#e64a19'},
            {hexCode: '#d84315'},
            {hexCode: '#bf360c'},
            {hexCode: '#ff9e80'},
            {hexCode: '#ff6e40'},
            {hexCode: '#ff3d00'},
            {hexCode: '#dd2c00'},
          ],
        },
        {
          name: 'Brown',
          baseColor: '#795548',
          color: [
            {hexCode: '#efebe9'},
            {hexCode: '#d7ccc8'},
            {hexCode: '#bcaaa4'},
            {hexCode: '#a1887f'},
            {hexCode: '#8d6e63'},
            {hexCode: '#795548'},
            {hexCode: '#6d4c41'},
            {hexCode: '#5d4037'},
            {hexCode: '#4e342e'},
            {hexCode: '#3e2723'},
          ],
        },
        {
          name: 'Gray',
          baseColor: '#9e9e9e',
          color: [
            {hexCode: '#fafafa'},
            {hexCode: '#f5f5f5'},
            {hexCode: '#eeeeee'},
            {hexCode: '#e0e0e0'},
            {hexCode: '#bdbdbd'},
            {hexCode: '#9e9e9e'},
            {hexCode: '#757575'},
            {hexCode: '#616161'},
            {hexCode: '#424242'},
            {hexCode: '#212121'},
          ],
        },
        {
          name: 'BlueGray',
          baseColor: '#607d8b',
          color: [
            {hexCode: '#eceff1'},
            {hexCode: '#cfd8dc'},
            {hexCode: '#b0bec5'},
            {hexCode: '#90a4ae'},
            {hexCode: '#78909c'},
            {hexCode: '#607d8b'},
            {hexCode: '#546e7a'},
            {hexCode: '#455a64'},
            {hexCode: '#37474f'},
            {hexCode: '#263238'},
          ],
        },
        {
          name: 'B&W',
          baseColor: 'black',
          color: [{hexCode: '#000000'}, {hexCode: '#ffffff'}],
        },
      ],
    };
  }

  copyToClipboard = colorCode => {
    Clipboard.setString(colorCode);
    ToastAndroid.show('Color Code Copied to Clipboard', ToastAndroid.SHORT);
  };

  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#610094'}}>
        <StatusBar backgroundColor="#610094" barStyle="light-content" />
        <Text style={styles.title}>Color Palette Hex Code</Text>
        <View style={styles.container}>
          <FlatGrid
            itemDimension={100}
            data={this.state.colorList}
            renderItem={({item, index}) => (
              <TouchableOpacity
                style={{
                  backgroundColor: item.baseColor,
                  height: 100,
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 20,
                }}
                onPress={() =>
                  this.setState({
                    modalVisible: true,
                    indexColor: index,
                  })
                }>
                <Text style={{color: '#ffffff', fontSize: 18}}>
                  {item.name}
                </Text>
              </TouchableOpacity>
            )}
          />
        </View>
        <Modal isVisible={this.state.modalVisible}>
          <View style={styles.modalContainer}>
            <StatusBar backgroundColor="#2A0944" barStyle="light-content" />
            <Text style={styles.modalTittle}>
              Tap untuk menyalin kode warna
            </Text>
            <FlatGrid
              itemDimension={150}
              data={this.state.colorList[this.state.indexColor].color}
              renderItem={({item}) => (
                <TouchableOpacity
                  style={{
                    backgroundColor: item.hexCode,
                    height: 100,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 20,
                  }}
                  onPress={() => this.copyToClipboard(item.hexCode)}>
                  <Text style={{color: '#ffffff', fontSize: 18}}>
                    {item.hexCode}
                  </Text>
                </TouchableOpacity>
              )}
            />
            <TouchableOpacity
              onPress={() => this.setState({modalVisible: false})}>
              <View style={{alignItems: 'center', marginVertical: 10}}>
                <Icon name="times-circle" size={40} color="#fff" />
              </View>
            </TouchableOpacity>
          </View>
        </Modal>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#2A0944',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    paddingTop: 10,
  },
  title: {
    textAlign: 'center',
    fontSize: 30,
    // fontWeight: 'bold',
    // fontFamily:''
    marginTop: 40,
    marginBottom: 60,
    color: '#ffffff',
  },
  modalTittle: {
    textAlign: 'center',
    color: '#fff',
    marginVertical: 10,
    fontSize: 17,
    fontWeight: 'bold',
  },
  modalContainer: {
    flex: 1,
    backgroundColor: '#2A0944',
    borderRadius: 20,
  },
});
export default App;
